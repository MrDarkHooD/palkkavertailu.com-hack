<?php
$jobs = explode("\n", file_get_contents("jobs.txt"));
$girls_names = explode("\n", file_get_contents("girls_names.txt"));
$boys_names = explode("\n", file_get_contents("boys_names.txt"));
$surnames = explode("\n", file_get_contents("surnames.txt"));
$citys = explode("\n", file_get_contents("citys.txt"));
$useragents = explode("\n", file_get_contents("useragents.txt"));
$realjobs = explode("\n", file_get_contents("realjobs.txt"));

array_pop($jobs);
array_pop($girls_names);
array_pop($boys_names);
array_pop($surnames);
array_pop($citys);
array_pop($useragents);
$error = 0;
while(1) {
    list($job, $salary, $direction, $percent) = explode(" | ", $jobs[rand(0, count($jobs))]);
    if($job && $salary && $percent) {
        $target_salary = ($direction) ? $salary+($percent/100*$salary) : $salary-($percent/100*$salary);
        $gender = (rand(1, 2));
        $employer_fn = (rand(0,1)) ? $boys_names[rand(0, count($boys_names))] : $girls_names[rand(0, count($girls_names))];
        $employers_name = $employer_fn . " " . $surnames[rand(0, count($surnames))];
        $city = $citys[rand(0, count($citys))];
        $useragent = $useragents[rand(0, count($useragents))];
        $experience = rand(0,30);
        $ip = rand(0,223).".".rand(0,255).".".rand(0,255).".".rand(0,255);
        $salary_to_post = (rand(0, 1)) ? $target_salary+(rand(1, 2)/100*$salary) : $target_salary-(rand(1, 2)/100*$salary);
        $salary_to_post = round($salary_to_post);
        
        if(rand(0,3) != 3) {
            foreach($realjobs as $realjob)
                if(substr($realjob, 0, strlen($job)) == $job) {
                    $workplaces = explode($job."|(", $realjob)[1];
                    $workplaces = rtrim($workplaces, ")");
                    $workplaces = explode("|",$workplaces);
                    $workplace = $workplaces[array_rand($workplaces)];
                    $workplace = str_replace($job." - ", "", $workplace);
                    if($workplace) $employers_name = $workplace;
                }
        }

        echo "Time and date now: ".date("d.m.Y H:i:s")."\n";
        echo "\033[38;5;214mNow handling\033[0m: $job\n";
        echo "\033[38;5;214mOriginal salary for this job\033[0m: {$salary}€\n";
        echo "\033[38;5;214mPercent of change\033[0m: ".(($direction) ? "+" : "-")."$percent%\n";
        echo "\033[38;5;214mTarget salary for this job\033[0m: {$target_salary}€\n";
        echo "\033[38;5;214mSalary to send\033[0m: {$salary_to_post}€\n";
        echo "\033[38;5;214mExperience years\033[0m: $experience\n";
        echo "\033[38;5;214mEmployers name\033[0m: $employers_name\n";
        echo "\033[38;5;214mCity\033[0m: $city\n";
        echo "\033[38;5;214mGender\033[0m: ".(($gender === 1) ? "Male" : "Female")."\n";
        echo "\033[38;5;214mUseragent\033[0m: $useragent\n";
        echo "\033[38;5;214mIP_address\033[0m: $ip\n";
        
        echo str_repeat("-", 50)."\n";
        echo "Sending data.\n";

        $post = [
            'title' => $job,
            'salary' => $salary_to_post,
            'company' => $employers_name,
            'experience' => $experience,
            'gender' => $gender,
            'city' => $city,
            'gd87' => 'Helsinki',];

        $ch = curl_init('https://palkkavertailu.com/add.php?done');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["REMOTE_ADDR: $ip",
                                               "HTTP_X_FORWARDED_FOR: $ip"]);
        $response = curl_exec($ch);
        curl_close($ch);
        echo "Data sent.\n";

        if(strstr($response, "<h1>Kiitos palkan lisäyksestä!</h1>")) {
            echo "\033[32mWe got thankyou message as responce, everything looks good.\033[0m\n";
            $error = 0;
        }else {
            echo "\033[31mNo thankyou message, something might be wrong.\033[0m\n";
            $error++;
        }

        if($error >= 3) die("\033[31m3 errors in row, shutting down.\033[0m\n");
        
        $sleep = rand(300,600);
        echo "Next message will be sent in ".round($sleep/60,1)." minutes.\n";
        echo "Time and date now: ".date("d.m.Y H:i:s")."\n";

        try {
            $logdata = "$job | $salary_to_post | $employers_name | $experience | ".(($gender === 1) ? "m" : "n" )." | $city\n";
            $fp = fopen('log.txt', 'a');
            fwrite($fp, $logdata);
            fclose($fp);
        }catch (Exception $e) {
            echo '\033[31mCaught exception while writing to log:\033[0m\n',  $e->getMessage(), "\n\n";
        }
        
        echo str_repeat("#", 50)."\n\n";
        sleep($sleep);
    }else {
        echo "\033[31mGetting random job failed\033[0m\n";
        echo "job: $job\n";
        echo "salary: $salary\n";
        echo "percent: $percent\n\n";
    }
}

